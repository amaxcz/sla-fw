import pydbus
import xml.etree.ElementTree as ET

# ONLY "HAPPY FLOW" CODE IS HERE.
# To check use Printer.hw.printer_model.options.*
# and self._printer.hw.printer_model.PrinterModel.M1


# See sla-fw/build/lib/slafw/hardware/sl1/printer_model.py to understand the class/meta hierarchy
MODEL = f"sl1"
SRV = f"cz.prusa3d.{MODEL}.admin0"


def show_menu(item: pydbus.SystemBus) -> list[str]:
    assert item is not None

    if item is None:
        return
    
    res = []
    print("=== Menu ===")
    for k, v in item.children:
        item = v.split('/')[-1]
        print(f"{item}")
        res.append(item)
    
    return res


def root_menu() -> pydbus.SystemBus:
    item = pydbus.SystemBus().get(SRV)
    content = item.Introspect()
    methods = ET.fromstring(content).findall(f".//interface[@name='{SRV}']/method")
    names = [x.get("name") for x in methods]
    if "enter" in names:
        item.enter()
    else:
        print(f"Available methods: {methods}")

    return item


def navigate_to(menu_item: str) -> pydbus.SystemBus:
    assert menu_item is not None
    item = pydbus.SystemBus().get(SRV, f"/cz/prusa3d/{MODEL}/admin0/Items/{menu_item}")
    content = item.Introspect()
    item.execute()
    return item


m = root_menu()
navigate_to("Firmware")
navigate_to("Firmware_tests")
navigate_to("Wizards_test")
items = show_menu(m)
assert "Display_test" in items


m = root_menu()
items = show_menu(m)
assert "Only_M1_feature" in items
navigate_to("Only_M1_feature")
items = show_menu(m)


